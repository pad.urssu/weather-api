package com.gausslab.weatherapi


const val MIN_LATITUDE = 33.0f
const val MAX_LATITUDE = 38.0f
const val MIN_LONGITUDE = 125.0f
const val MAX_LONGITUDE = 130.0f


val ONE_DAY_IN_SECONDS: Long = 86399L
val CLOSESTPOINT_SEARCH_RADIUS = 0.045f // 5KM
val TIMESTAMP_REGEX_PATTERN = "\\d{4}-\\d{2}-\\d{2}".toRegex() // The timestamp format is "yyyy-MM-dd"
val FORECASTED_TIME_REGEX_PATTERN = """^([01]?[0-9]|2[0-3]):[0-5][0-9]$""".toRegex() // 00:00~23:59
const val HISTORICAL_MIN_DATE = "1970-01-01"