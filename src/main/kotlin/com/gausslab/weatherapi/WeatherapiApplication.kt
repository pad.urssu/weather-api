package com.gausslab.weatherapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class WeatherapiApplication

fun main(args: Array<String>) {
    runApplication<WeatherapiApplication>(*args)
}
