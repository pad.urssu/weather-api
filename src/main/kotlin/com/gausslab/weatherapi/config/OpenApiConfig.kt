package com.gausslab.weatherapi.config

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfig {
    @Bean
    fun openAPI(@Value("\${springdoc.version}") springdocVersion: String): OpenAPI {
        val info = Info()
                .title("Weather API")
                .version(springdocVersion)
                .description("기상 API 문서입니다.")

        val securityScheme = SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .scheme("bearer")
                .bearerFormat("JWT")

        return OpenAPI()
                .components(Components()
                        .addSecuritySchemes("Authorization", securityScheme))
                .info(info)
                .addSecurityItem(SecurityRequirement().addList("Authorization"))
    }
}
