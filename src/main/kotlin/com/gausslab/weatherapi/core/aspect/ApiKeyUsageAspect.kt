package com.gausslab.weatherapi.core.aspect

import com.gausslab.weatherapi.core.entity.ApiKey
import com.gausslab.weatherapi.core.enums.ApiKeyStatus
import com.gausslab.weatherapi.core.service.ApiKeyService
import com.gausslab.weatherapi.core.util.ThrottleStore
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

@Aspect
@Component
class ApiKeyUsageAspect(
        private val apiKeyService: ApiKeyService,
        private val throttleStore: ThrottleStore,
        @Value("\${app.request.duplicate-reject-period-ms}") private val requestInterval: Long,
) {

    @Around("execution(* com.gausslab.weatherapi.datafetch.controller.WeatherInfoRESTController.*(..))")
    fun handleApiKeyUsage(joinPoint: ProceedingJoinPoint): Any? {
        val request = (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes).request
        val apiKey = request.getHeader("X-Api-Key")
                ?: return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"API key is missing\",\"code\":\"INVALID_API_KEY\"}")

        if (!apiKeyService.validateKey(apiKey)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"error\":\"Invalid API key\",\"code\":\"UNAUTHORIZED\"}")
        }
        if (apiKeyService.checkStatus(apiKey) != ApiKeyStatus.ACTIVE) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"error\":\"Invalid API key\",\"code\":\"API KEY STATUS IS INACTIVE OR SUSPENDED\"}")
        }

        val queryParams = request.queryString ?: ""

        if (throttleStore.shouldThrottle(apiKey, "getHistoricalForecastWeatherInfo_REST", queryParams)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).body("{\"error\":\"Too many requests. Please wait for "+requestInterval.toString()+" ms.\"}")
        }
        val result = joinPoint.proceed()

        val key: ApiKey = apiKeyService.getKey(apiKey)
        apiKeyService.increaseUsage(key)

        return result
    }
}
