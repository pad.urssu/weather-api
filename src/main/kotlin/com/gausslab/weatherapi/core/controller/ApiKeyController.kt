package com.gausslab.weatherapi.core.controller

import com.gausslab.weatherapi.core.dto.ErrorDto
import com.gausslab.weatherapi.core.response.ApiKeyResponse
import com.gausslab.weatherapi.core.response.ApiKeyUsageResponse
import com.gausslab.weatherapi.core.service.ApiKeyService
import com.gausslab.weatherapi.core.security.UserPrincipal
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api")
class ApiKeyController(private val apiKeyService: ApiKeyService) {
    @PostMapping("/keys")
    @Operation(summary = "API 키 생성", description = "인증된 사용자에게 API 키를 생성하고 반환합니다.")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "성공적으로 API 키 생성",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ApiKeyResponse::class))]),
        ApiResponse(responseCode = "401", description = "JWT 인증 오류",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
    ])
    fun generateKey(
            @AuthenticationPrincipal
            @Parameter(description = "인증된 사용자 정보", hidden = true)
            userPrincipal: UserPrincipal
    ): ResponseEntity<ApiKeyResponse> {
        val apiKey = apiKeyService.generateAndAssignKey(userPrincipal.username)
        println("API key generated: ${apiKey.keyHash}")
        return ResponseEntity.ok(
                ApiKeyResponse(
                        apiKey.keyHash,
                        apiKey.apiKeyStatus.toString(),
                        "Generated",
                )
        )
    }

    @DeleteMapping("/keys")
    @Operation(summary = "특정 API key 지우기")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "API key 삭제 성공",
                content = [Content(schema = Schema(implementation = ApiKeyResponse::class))]),
        ApiResponse(responseCode = "401", description = "JWT 인증 오류",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
    ])
    fun deleteKey(
            @AuthenticationPrincipal userPrincipal: UserPrincipal,
            @Parameter(description = "지울 API 키", required = true)
            @RequestParam key: String
    ): ResponseEntity<ApiKeyResponse> {
        val deleted = apiKeyService.deleteKey(userPrincipal.username, key)
        return ResponseEntity.ok(
                ApiKeyResponse(
                        deleted.keyHash,
                        deleted.apiKeyStatus.toString(),
                        "API key deleted successfully"
                )
        )
    }

    @GetMapping("/keys")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "API key 전부 불러오기 성공",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                        schema = Schema(type = "array", implementation = ApiKeyResponse::class))]),
        ApiResponse(responseCode = "401", description = "JWT 인증 오류",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
    ])
    fun getKeys(
            @AuthenticationPrincipal userPrincipal: UserPrincipal
    ): ResponseEntity<List<ApiKeyResponse>> {
        val keys = apiKeyService.getKeys(userPrincipal.username)
        val keyList = keys.map {
            ApiKeyResponse(
                    it.keyHash,
                    it.apiKeyStatus.toString()
            )
        }
        return ResponseEntity.ok(keyList)
    }

    @GetMapping("/keys/{key}")
    @Operation(summary = "특정 키의 사용내역 불러오기")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "API key 사용내역 불러오기 성공",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                        schema = Schema(type = "array", implementation = ApiKeyUsageResponse::class))]),
        ApiResponse(responseCode = "401", description = "JWT 인증 오류",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
        ApiResponse(responseCode = "404", description = "API key를 찾을 수 없음"),
    ])
    fun getKey(
            @PathVariable key: String
    ): ResponseEntity<List<ApiKeyUsageResponse>> {
        val keyUsage = apiKeyService.getKeyUsage(key)
        return ResponseEntity.ok(keyUsage.map {
            ApiKeyUsageResponse(
                    it.apiKeyUsageId,
                    it.usageDate
            )
        })
    }

    @PatchMapping("/keys/{key}")
    @Operation(summary = "특정 api 키 비활성화")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "API key inactivated 성공",
                content = [Content(schema = Schema(implementation = ApiKeyResponse::class))]),
        ApiResponse(responseCode = "401", description = "JWT 인증 오류",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
        ApiResponse(responseCode = "404", description = "API key를 찾을 수 없음"),
    ])
    fun deactivateKey(
            @PathVariable key: String
    ): ResponseEntity<ApiKeyResponse> {
        val inactive = apiKeyService.setApiKeyStatusToInactive(key) ?: throw Exception("API key not found")
        return ResponseEntity.ok().body(
                ApiKeyResponse(
                        inactive.keyHash,
                        inactive.apiKeyStatus.toString(),
                        "API key inactivated successfully"
                )
        )
    }

    @ExceptionHandler(Exception::class)
    fun handleException(
            e: Exception
    ): ResponseEntity<String> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.message)
    }

    @ExceptionHandler(SecurityException::class)
    fun handleSecurityException(
            e: SecurityException
    ): ResponseEntity<String> {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.message)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    fun handleIllegalArgumentException(
            e: IllegalArgumentException
    ): ResponseEntity<String> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.message)
    }
}


