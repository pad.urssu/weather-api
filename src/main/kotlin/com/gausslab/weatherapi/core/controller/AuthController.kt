package com.gausslab.weatherapi.core.controller;

import com.gausslab.weatherapi.core.dto.ErrorDto;
import com.gausslab.weatherapi.core.request.SignInRequestDto;
import com.gausslab.weatherapi.core.response.SignInResponseDto;
import com.gausslab.weatherapi.core.request.SignUpRequestDto;
import com.gausslab.weatherapi.core.response.SignUpResponseDto;
import com.gausslab.weatherapi.core.response.UserResponseDto;
import com.gausslab.weatherapi.core.service.UserService;
import com.gausslab.weatherapi.core.security.UserPrincipal;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class AuthController(
        private val userService: UserService
) {
    @PostMapping("/register")
    @Operation(summary = "회원 가입", description = "새로운 사용자를 등록합니다.")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "성공적으로 사용자 생성",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = SignUpResponseDto::class))]),
        ApiResponse(responseCode = "400", description = "잘못된 요청",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
    ])
    fun signUp(
            @RequestBody @Parameter(description = "회원 가입 정보", required = true) signUpBody: SignUpRequestDto
    ): ResponseEntity<SignUpResponseDto> {
        val result = userService.signUp(signUpBody);
        val response = SignUpResponseDto(result);
        return ResponseEntity.ok(response);
    }

    //login
    @PostMapping("/login")
    @Operation(summary = "로그인", description = "사용자 로그인을 처리합니다.")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "성공적으로 로그인",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = SignInResponseDto::class))]),
        ApiResponse(responseCode = "400", description = "잘못된 요청",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
        ApiResponse(responseCode = "401", description = "인증 실패",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
    ])
    fun signIn(
            @RequestBody @Parameter(description = "로그인 정보", required = true) signInBody: SignInRequestDto
    ): ResponseEntity<SignInResponseDto> {
        val result = userService.signIn(signInBody);
        val response = SignInResponseDto(result);
        return ResponseEntity.ok(response);
    }

    //사용자 정보 가져오기
    @GetMapping("/user")
    @Operation(summary = "로그인한 사용자 정보 가져오기", description = "현재 로그인한 사용자의 정보를 반환합니다.")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "성공적으로 사용자 정보 조회",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = UserResponseDto::class))]),
        ApiResponse(responseCode = "401", description = "JWT 인증 오류",
                content = [Content(mediaType = "application/json",
                        schema = Schema(implementation = ErrorDto::class))]),
    ])
    fun getUser(
            @AuthenticationPrincipal @Parameter(description = "인증된 사용자 정보", hidden = true) userPrincipal: UserPrincipal
    ): ResponseEntity<UserResponseDto> {
        val result = userService.getUser(userPrincipal.username);
        val response = UserResponseDto(result);
        return ResponseEntity.ok(response);
    }
}
