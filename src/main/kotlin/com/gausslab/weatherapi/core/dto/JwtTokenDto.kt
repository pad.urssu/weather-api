package com.gausslab.weatherapi.core.dto

class JwtTokenDto(
        val token: String,
        val expiredIn: Long
)