package com.gausslab.weatherapi.core.dto

import com.gausslab.weatherapi.core.entity.User


class UserDto(
        val userId: Long,
        val displayName: String,
        val email: String,
        val password: String,
        val accessToken: String,
        val credit: Long
){
    constructor(user: User, jwt:String): this(
            userId = user.userId,
            displayName = user.displayName,
            email = user.userEmail,
            password = user.password,
            accessToken = jwt,
            credit = user.credit
    )
}