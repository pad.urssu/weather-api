package com.gausslab.weatherapi.core.entity

import com.gausslab.weatherapi.core.enums.ApiKeyStatus
import jakarta.persistence.*

@Entity
class ApiKey(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val apiKeyId: Long = 0,
        val keyHash: String,
        @ManyToOne
        @JoinColumn(name = "user_id")
        val user: User,
        @Column(nullable = false)
        var apiKeyStatus: ApiKeyStatus = ApiKeyStatus.ACTIVE,
)