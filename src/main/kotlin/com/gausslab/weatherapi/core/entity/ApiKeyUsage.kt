package com.gausslab.weatherapi.core.entity

import jakarta.persistence.*

@Entity
class ApiKeyUsage(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val apiKeyUsageId: Long =0,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "api_key_id")
        val apiKey: ApiKey,

        @Column(nullable = false)
        val usageDate: Long = 0,
)