package com.gausslab.weatherapi.core.entity

data class ForecastWeatherInfoId(
        val timestamp: Long = 0,
        val latitude: Float = 0.0f,
        val longitude: Float = 0.0f,
        val forecastedTime: Long = 0
) : java.io.Serializable
