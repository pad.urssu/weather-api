package com.gausslab.weatherapi.core.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.IdClass


@Entity
@IdClass(HistoricalWeatherInfoId::class)
class HistoricalWeatherInfo(
        @Id
        @Column(columnDefinition = "INT")
        val timestamp: Long = 0,
        @Id
        @Column(columnDefinition = "DECIMAL(6,3)")
        val latitude: Float = 0.0f,
        @Id
        @Column(columnDefinition = "DECIMAL(6,3)")
        val longitude: Float = 0.0f,

        val temperature: String = "",
        val apparent_temperature: String = "",
        val soil_temperature: String = "",
        val relative_humidity: String = "",
        val dewpoint: String = "",
        val pressure: String = "",
        val surface_pressure: String = "",
        val cloudcover: String = "",
        val cloudcover_low: String = "",
        val cloudcover_mid: String = "",
        val cloudcover_high: String = "",
        val wind_speed: String = "",
        val wind_speed_high: String = "",
        val wind_direction: String = "",
        val wind_direction_high: String = "",
        val wind_gust_speed: String = "",
        val global_horizontal_irradiance: String = "",
        val direct_irradiance: String = "",
        val direct_normal_irradiance: String = "",
        val diffuse_horizontal_irradiance: String = "",
        val precipitation: String = "",
        val snow: String = "",
        val rain: String = ""
)