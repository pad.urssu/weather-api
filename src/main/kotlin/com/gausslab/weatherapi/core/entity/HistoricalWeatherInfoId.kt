package com.gausslab.weatherapi.core.entity

data class HistoricalWeatherInfoId(
        val timestamp: Long = 0,
        val latitude: Float = 0.0f,
        val longitude: Float = 0.0f,
) : java.io.Serializable