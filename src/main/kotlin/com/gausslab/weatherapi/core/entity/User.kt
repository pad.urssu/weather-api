package com.gausslab.weatherapi.core.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import org.springframework.security.core.userdetails.UserDetails

@Entity
class User(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val userId: Long = 0,
        @Column(unique = true)
        val displayName: String,
        @Column(unique = true)
        val userEmail: String,
        val userPassword: String,
        //TODO: 사용량 추가
        @OneToMany(mappedBy = "user")
        val apiKeys: List<ApiKey> = emptyList(),
        val credit: Long = 0
) : UserDetails {
    override fun getAuthorities() = null

    override fun getPassword() = userPassword

    override fun getUsername() = displayName

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true
}