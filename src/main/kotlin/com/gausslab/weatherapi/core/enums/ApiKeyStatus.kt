package com.gausslab.weatherapi.core.enums

enum class ApiKeyStatus {
    ACTIVE, INACTIVE, SUSPENDED
}