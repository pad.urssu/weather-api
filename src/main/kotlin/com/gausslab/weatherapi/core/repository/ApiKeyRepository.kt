package com.gausslab.weatherapi.core.repository

import com.gausslab.weatherapi.core.entity.ApiKey
import com.gausslab.weatherapi.core.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface ApiKeyRepository: JpaRepository<ApiKey, Long>{
    fun existsByKeyHash(key: String): Boolean
    fun findByKeyHashAndUser(key: String, user: User): ApiKey?
    fun findByKeyHash(key: String): ApiKey
    fun findAllByUser(user: User): List<ApiKey>
}