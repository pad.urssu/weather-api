package com.gausslab.weatherapi.core.repository

import com.gausslab.weatherapi.core.entity.ApiKey
import com.gausslab.weatherapi.core.entity.ApiKeyUsage
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository

interface ApiKeyUsageRepository: JpaRepository<ApiKeyUsage, Long> {
    fun findAllByApiKey(key: ApiKey, sort: Sort): List<ApiKeyUsage>
}