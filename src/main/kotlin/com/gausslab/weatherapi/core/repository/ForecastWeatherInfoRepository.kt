package com.gausslab.weatherapi.core.repository

import com.gausslab.weatherapi.core.entity.ForecastWeatherInfo
import com.gausslab.weatherapi.core.entity.ForecastWeatherInfoId
import com.gausslab.weatherapi.datafetch.dto.CoordinatesRange
import com.gausslab.weatherapi.datafetch.dto.TimestampRange
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ForecastWeatherInfoRepository : JpaRepository<ForecastWeatherInfo, ForecastWeatherInfoId> {

    @Query("""
    SELECT w
    FROM ForecastWeatherInfo w
    WHERE w.timestamp BETWEEN :startDate AND :endDate
    AND w.latitude BETWEEN :minLatitude AND :maxLatitude
    AND w.longitude BETWEEN :minLongitude AND :maxLongitude
""")

    fun findWithinBoundingBox(
        startDate: Long,
        endDate: Long,
        minLatitude: Float,
        maxLatitude: Float,
        minLongitude: Float,
        maxLongitude: Float
    ): List<ForecastWeatherInfo>


    @Query(value = """
    SELECT * FROM forecast_weather_info w
    WHERE w.latitude = :latitude
    AND w.longitude = :longitude
    AND w.timestamp BETWEEN :startDate AND :endDate
    AND w.forecasted_time IN (
        SELECT MAX(forecasted_time)
        FROM forecast_weather_info
        WHERE latitude = :latitude
        AND longitude = :longitude
        AND timestamp BETWEEN :startDate AND :endDate
        AND forecasted_time <= :requestedSearchTime
        GROUP BY timestamp
    )
""", nativeQuery = true)
    fun findRelevantWeatherInfoFromRequestedSearchTime(
        latitude: Float,
        longitude: Float,
        startDate: Long,
        endDate: Long,
        requestedSearchTime: Long
    ): List<ForecastWeatherInfo>

    @Query("""
    SELECT new com.gausslab.weatherapi.datafetch.dto.CoordinatesRange(
        MIN(w.latitude), MAX(w.latitude),
        MIN(w.longitude), MAX(w.longitude))
        FROM ForecastWeatherInfo w
""")

    fun findMinMaxCoordinates(): CoordinatesRange

    @Query("""
        SELECT new com.gausslab.weatherapi.datafetch.dto.TimestampRange(
        MIN(w.timestamp), MAX(w.timestamp))
        FROM ForecastWeatherInfo w
""")
    fun findMinMaxTimestamp(): TimestampRange

}