package com.gausslab.weatherapi.core.repository

import com.gausslab.weatherapi.core.entity.HistoricalWeatherInfo
import com.gausslab.weatherapi.core.entity.HistoricalWeatherInfoId
import com.gausslab.weatherapi.datafetch.dto.CoordinatesRange
import com.gausslab.weatherapi.datafetch.dto.TimestampRange
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface HistoricalWeatherInfoRepository : JpaRepository<HistoricalWeatherInfo, HistoricalWeatherInfoId> {
    fun findAllByLatitudeEqualsAndLongitudeEqualsAndTimestampBetween(
            latitude: Float,
            longitude: Float,
            startDate: Long,
            endDate: Long
    ): List<HistoricalWeatherInfo>

    @Query("""
    SELECT new com.gausslab.weatherapi.datafetch.dto.CoordinatesRange(
        MIN(w.latitude), MAX(w.latitude),
        MIN(w.longitude), MAX(w.longitude))
        FROM HistoricalWeatherInfo w
""")

    fun findMinMaxCoordinates(): CoordinatesRange

    @Query("""
        SELECT new com.gausslab.weatherapi.datafetch.dto.TimestampRange(
        MIN(w.timestamp), MAX(w.timestamp))
        FROM HistoricalWeatherInfo w
""")
    fun findMinMaxTimestamp(): TimestampRange
}