package com.gausslab.weatherapi.core.repository

import com.gausslab.weatherapi.core.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<User, Long> {
    fun findByUserEmail(email: String): User?
}