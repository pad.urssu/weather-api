package com.gausslab.weatherapi.core.request

class SignInRequestDto(
        val email: String,
        val password: String,
)