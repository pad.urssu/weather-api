package com.gausslab.weatherapi.core.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class ApiKeyResponse(
        val key: String,
        val status: String = "Active",
        val message: String? = null,
)