package com.gausslab.weatherapi.core.response

class ApiKeyUsageResponse(
        val apiKeyUsageId: Long,
        val usageDate: Long
)