package com.gausslab.weatherapi.core.response

import com.gausslab.weatherapi.core.dto.UserDto


class SignInResponseDto(
        val userId: Long,
        val displayName: String,
        val email: String,
        val accessToken: String,
        val credit: Long
){
    constructor( user: UserDto): this(
            userId = user.userId,
            displayName = user.displayName,
            email = user.email,
            accessToken = user.accessToken,
            credit = user.credit
    )
}
