package com.gausslab.weatherapi.core.response

import com.gausslab.weatherapi.core.dto.UserDto

class SignUpResponseDto(
        val userId: Long,
        val displayName: String,
        val email: String,
        val accessToken: String,
){
    constructor( user: UserDto): this(
            userId = user.userId,
            displayName = user.displayName,
            email = user.email,
            accessToken = user.accessToken
    )
}
