package com.gausslab.weatherapi.core.response

import com.gausslab.weatherapi.core.dto.UserDto

class UserResponseDto(
        val userId: Long,
        val displayName: String,
        val email: String,
        val credit: Long
) {
    constructor(user: UserDto) : this(
            userId = user.userId,
            displayName = user.displayName,
            email = user.email,
            credit = user.credit
    )
}
