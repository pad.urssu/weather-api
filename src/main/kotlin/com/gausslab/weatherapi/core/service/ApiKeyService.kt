package com.gausslab.weatherapi.core.service

import com.gausslab.weatherapi.core.entity.ApiKey
import com.gausslab.weatherapi.core.entity.ApiKeyUsage
import com.gausslab.weatherapi.core.enums.ApiKeyStatus
import com.gausslab.weatherapi.core.repository.ApiKeyRepository
import com.gausslab.weatherapi.core.repository.ApiKeyUsageRepository
import org.springframework.beans.factory.ObjectProvider
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*

@Service
class ApiKeyService(
        private val apiKeyRepository: ApiKeyRepository,
        private val apiKeyUsageRepository: ApiKeyUsageRepository,
        private val userServiceProvider: ObjectProvider<UserService>,
        ) {
    private val userService: UserService by lazy { userServiceProvider.getObject() }
    fun generateAndAssignKey(email: String): ApiKey {
        val user = userService.getUserFromEmail(email)
        val key = UUID.randomUUID().toString()
        val apiKey = ApiKey(keyHash = key, user = user)
        return apiKeyRepository.save(apiKey)
    }

    fun checkStatus(key: String): ApiKeyStatus {
        val apiKey = apiKeyRepository.findByKeyHash(key)
                ?: throw IllegalArgumentException("Invalid API key")
        return apiKey.apiKeyStatus
    }

    fun validateKey(key: String): Boolean {
        return apiKeyRepository.existsByKeyHash(key)
    }

    //ApiKeyUsage 객체 생성 및 저장
    fun increaseUsage(key: ApiKey) {
        val epochTime = System.currentTimeMillis() / 1000
        apiKeyUsageRepository.save(
                //usageDate에 epochtime 넣음
                ApiKeyUsage(apiKey = key, usageDate = epochTime)
        )
    }

    fun deleteKey(email: String, key: String): ApiKey {
        val user = userService.getUserFromEmail(email)
        val apiKey = apiKeyRepository.findByKeyHashAndUser(key, user)
                ?: throw IllegalArgumentException("Invalid API key")
        apiKeyRepository.delete(apiKey)
        return apiKey
    }

    fun getKeys(email: String): List<ApiKey> {
        val user = userService.getUserFromEmail(email)
        return apiKeyRepository.findAllByUser(user)
    }

    fun getKey(key: String): ApiKey {
        return apiKeyRepository.findByKeyHash(key)
    }

    fun getKeyUsage(key: String): List<ApiKeyUsage> {
        val apiKey = apiKeyRepository.findByKeyHash(key)
        val sort = Sort.by(Sort.Direction.DESC, "usageDate")
        return apiKeyUsageRepository.findAllByApiKey(apiKey, sort)
    }

    fun setApiKeyStatusToInactive(key: String): ApiKey? {
        val apiKey: ApiKey
        try {
            apiKey = apiKeyRepository.findByKeyHash(key)
            apiKey.apiKeyStatus = ApiKeyStatus.INACTIVE
            apiKeyRepository.save(apiKey)
        } catch (e: Exception) {
            return null
        }
        return apiKey
    }
}
