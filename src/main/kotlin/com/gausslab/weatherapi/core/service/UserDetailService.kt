package com.gausslab.weatherapi.core.service

import com.gausslab.weatherapi.core.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class UserDetailService(
        private val userRepository: UserRepository
): UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        return userRepository.findByUserEmail(username)
                ?: throw IllegalArgumentException("User not found")
    }
}

