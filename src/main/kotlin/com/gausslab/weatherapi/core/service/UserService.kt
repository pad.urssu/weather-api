package com.gausslab.weatherapi.core.service

import com.gausslab.weatherapi.core.request.SignInRequestDto
import com.gausslab.weatherapi.core.request.SignUpRequestDto
import com.gausslab.weatherapi.core.dto.UserDto
import com.gausslab.weatherapi.core.entity.User
import com.gausslab.weatherapi.core.repository.UserRepository
import com.gausslab.weatherapi.core.security.JwtTokenProvider
import jakarta.transaction.Transactional
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService(
        private val userRepository: UserRepository,
        private val passwordEncoder: PasswordEncoder,
        private val jwtTokenProvider: JwtTokenProvider
) {
    @Transactional
    fun signUp(dto: SignUpRequestDto): UserDto {
        val savedUser = userRepository.save(
                User(
                        displayName = dto.nickname,
                        userEmail = dto.email,
                        userPassword = passwordEncoder.encode(dto.password),
                )
        )
        return UserDto(savedUser, jwtTokenProvider.generateAccessToken(dto.email).token)
    }

    @Transactional
    fun signIn(dto: SignInRequestDto): UserDto {
        val user = userRepository.findByUserEmail(dto.email)
                ?: throw IllegalArgumentException("User not found")
        if (!passwordEncoder.matches(dto.password, user.password)) {
            throw IllegalArgumentException("Password is incorrect")
        }
        return UserDto(user, jwtTokenProvider.generateAccessToken(dto.email).token)
    }

    //jwt 토큰을 통해 User 정보를 가져오는 함수
    fun getUserFromToken(token: String): User {
        val email = jwtTokenProvider.getUserEmailFromToken(token)
        return userRepository.findByUserEmail(email)
                ?: throw IllegalArgumentException("User not found")
    }

    fun getUserFromEmail(username: String): User {
        return userRepository.findByUserEmail(username)
                ?: throw IllegalArgumentException("User not found")
    }

    fun getUser(username: String): UserDto {
        val user = userRepository.findByUserEmail(username)
                ?: throw IllegalArgumentException("User not found")
        return UserDto(user, jwtTokenProvider.generateAccessToken(username).token)
    }
}