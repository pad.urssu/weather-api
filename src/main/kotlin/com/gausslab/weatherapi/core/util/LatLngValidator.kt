package com.gausslab.weatherapi.core.util

object LatLngValidator {
    private const val MIN_LATITUDE = -90.0f
    private const val MAX_LATITUDE = 90.0f
    private const val MIN_LONGITUDE = -180.0f
    private const val MAX_LONGITUDE = 180.0f

    fun isCoordinatesValid(latitude: Float, longitude: Float): Boolean{
        return isLatitudeValid(latitude) && isLongitudeValid(longitude)
    }

    fun isLatitudeValid(latitude: Float): Boolean{
        if (latitude < MIN_LATITUDE || latitude > MAX_LATITUDE) {
            throw IllegalArgumentException("Latitude is out of range")
        }
        else if (latitude.isNaN()){
            throw IllegalArgumentException("Latitude is not a number")
        }
        return true
    }

    fun isLongitudeValid(longitude: Float):Boolean {
        if (longitude < MIN_LONGITUDE || longitude > MAX_LONGITUDE) {
            throw IllegalArgumentException("Longitude is out of range")
        }
        else if (longitude.isNaN()){
            throw IllegalArgumentException("Longitude is not a number")
        }
        return true
    }
}