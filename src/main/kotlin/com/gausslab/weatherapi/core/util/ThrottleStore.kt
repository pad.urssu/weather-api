package com.gausslab.weatherapi.core.util

import jakarta.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.TaskScheduler
import org.springframework.stereotype.Component
import java.util.Date
import java.util.concurrent.ConcurrentHashMap

@Component
class ThrottleStore(
        @Value("\${app.request.duplicate-cache-clear-interval-ms}") private val clearIntervalMS: Long,
        @Value("\${app.request.duplicate-reject-period-ms}") private val periodMS: Long,
        private val taskScheduler: TaskScheduler
) {
    private val requestTimes: ConcurrentHashMap<String, Long> = ConcurrentHashMap()

    fun shouldThrottle(apiKey: String, endpoint: String, queryParams: String): Boolean {
        val currentTimestamp = System.currentTimeMillis()
        val key = "$apiKey-$endpoint-$queryParams"
        val lastRequestTime = requestTimes[key]

        return if (lastRequestTime == null || currentTimestamp - lastRequestTime > periodMS) {
            requestTimes[key] = currentTimestamp
            false
        } else {
            true
        }
    }

    @PostConstruct
    private fun initScheduler() {
        taskScheduler.scheduleAtFixedRate(::clearOldEntries, Date(), clearIntervalMS)
    }

    private fun clearOldEntries() {
        val currentTimestamp = System.currentTimeMillis()
        requestTimes.entries.removeIf { currentTimestamp - it.value > periodMS }
    }
}
