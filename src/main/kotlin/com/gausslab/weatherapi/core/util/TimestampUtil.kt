package com.gausslab.weatherapi.core.util

import java.time.*
import java.time.format.DateTimeParseException

object TimestampUtil {
    fun isTimestampValid(timestamp: String): Boolean {
        val epochStartingPoint = 0 // unix epoch time은 1970년 1월 1일을 기준으로 0부터 시작
        val dateStartingPoint = LocalDate.parse("1970-01-01") // unix epoch time이랑 맞추기위해 1970년 1월 1일을 기준으로 시작
        val dateStartingPointLimit = LocalDate.parse("2038-01-19") //32bit epoch time한계

        if (timestamp.matches("\\d{4}-\\d{2}-\\d{2}".toRegex())) {
            // The timestamp format is "yyyy-MM-dd"
            try {
                LocalDate.parse(timestamp)
                if(LocalDate.parse(timestamp) < dateStartingPoint || LocalDate.parse(timestamp)> dateStartingPointLimit){
                    throw IllegalArgumentException("Timestamp is out of range")
                }
                return true
            } catch (e: DateTimeParseException) {
                throw IllegalArgumentException("Invalid timestamp format")
            } catch (e: Exception) {
                throw IllegalArgumentException("Invalid timestamp")
            }
        } else {
            // The timestamp format is assumed to be "1674288000"
            try {
                val timestampValue = timestamp.toLong()
                if (timestampValue < epochStartingPoint) {
                    throw IllegalArgumentException("Timestamp is out of range")
                }
                return true
            } catch (e: NumberFormatException) {
                throw IllegalArgumentException("Invalid timestamp format")
            }
        }
    }
    fun convertDateLongAndTimeStringToEpochTime(epochDate: Long, forecastedTime: String): Long {
        val (hour, minute) = forecastedTime.split(":").map { it.toInt() }

        val dateInstant = Instant.ofEpochSecond(epochDate)
        val localDateTime = dateInstant.atZone(ZoneOffset.UTC).toLocalDateTime()
        val combinedDateTime = localDateTime.withHour(hour).withMinute(minute)

        return combinedDateTime.toInstant(ZoneOffset.UTC).epochSecond
    }

    fun String.parseDateToEpochSeconds(zoneId: ZoneId = ZoneOffset.UTC): Long {
        return try {
            val date = LocalDate.parse(this)
            date.atStartOfDay(zoneId).toEpochSecond()
        } catch (e: DateTimeParseException) {
            throw IllegalArgumentException("Invalid timestamp format")
        } catch (e: Exception) {
            throw IllegalArgumentException("Invalid timestamp")
        }
    }

    fun String.toStartOfDayEpochSeconds(zoneId: ZoneId = ZoneOffset.UTC): Long {
        return try {
            val timestamp = this.toLong()

            if (timestamp < Int.MIN_VALUE || timestamp > Int.MAX_VALUE) {
                throw IllegalArgumentException("Timestamp is out of range")
            }

            val dateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), zoneId)
            val dateOnly = dateTime.toLocalDate().atStartOfDay(zoneId)
            dateOnly.toEpochSecond()

        } catch (e: NumberFormatException) {
            throw IllegalArgumentException("Invalid timestamp format")
        }
    }
}