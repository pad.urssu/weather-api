package com.gausslab.weatherapi.core.util

import com.gausslab.weatherapi.FORECASTED_TIME_REGEX_PATTERN
import com.gausslab.weatherapi.ONE_DAY_IN_SECONDS
import com.gausslab.weatherapi.TIMESTAMP_REGEX_PATTERN
import com.gausslab.weatherapi.datafetch.service.DBDataService
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeParseException

object WeatherInfoValidator {

    private val dateStartingPoint = LocalDate.parse(convertTimestampToDateString(DBDataService.FORECAST_MIN_TIMESTAMP - ONE_DAY_IN_SECONDS)) // DB의 MIN_TIMESTAMP사용
    val dateStartingPointLimit: LocalDate = LocalDate.parse(convertTimestampToDateString(DBDataService.FORECAST_MAX_TIMESTAMP)) // DB의 MAX_TIMESTAMP사용

    fun isCoordinatesValid(latitude: Float, longitude: Float): Boolean {
        if (!LatLngValidator.isCoordinatesValid(latitude, longitude)) {
            throw IllegalArgumentException("Coordinate values are incorrect")
        }
        return isLatitudeValid(latitude) && isLongitudeValid(longitude)
    }

    fun isLatitudeValid(latitude: Float): Boolean {
        if (latitude < DBDataService.FORECAST_MIN_LATITUDE || latitude > DBDataService.FORECAST_MAX_LATITUDE) {
            throw IllegalArgumentException("Latitude is out of range")
        }
        return true
    }

    fun isLongitudeValid(longitude: Float): Boolean {
        if (longitude < DBDataService.FORECAST_MIN_LONGITUDE || longitude > DBDataService.FORECAST_MAX_LONGITUDE) {
            throw IllegalArgumentException("Longitude is out of range")
        }
        return true
    }

    fun isTimestampValid(timestamp: String): Boolean {
        if (TimestampUtil.isTimestampValid(timestamp)) {
            if (timestamp.matches(TIMESTAMP_REGEX_PATTERN)) {
                // The timestamp format is "yyyy-MM-dd"
                try {
                    LocalDate.parse(timestamp)
                    if (LocalDate.parse(timestamp) < dateStartingPoint || LocalDate.parse(timestamp) > dateStartingPointLimit) {
                        throw IllegalArgumentException("Timestamp is out of range")
                    }
                    return true
                } catch (e: DateTimeParseException) {
                    throw IllegalArgumentException("Invalid timestamp format")
                } catch (e: Exception) {
                    throw IllegalArgumentException("Invalid timestamp")
                }
            } else {
                // The timestamp format is assumed to be "1674288000"
                try {
                    val timestampValue = timestamp.toLong()
                    if (timestampValue < DBDataService.FORECAST_MIN_TIMESTAMP || timestampValue > DBDataService.FORECAST_MAX_TIMESTAMP) {
                        throw IllegalArgumentException("Timestamp is out of range")
                    }
                    return true
                } catch (e: NumberFormatException) {
                    throw IllegalArgumentException("Invalid timestamp format")
                }
            }
        } else {
            return false
        }
    }

    fun isDateValid(startDate: Long, endDate: Long): Boolean {
        return startDate <= endDate
    }

    fun convertTimestampToDateString(timestamp: Long): String {
        val instant = Instant.ofEpochSecond(timestamp)
        val localDate = instant.atZone(ZoneId.of("GMT")).toLocalDate()
        return localDate.toString()
    }

    fun isForecastedTimeValid(forecastedTime: String): Boolean {
        return FORECASTED_TIME_REGEX_PATTERN.matches(forecastedTime)
    }

}