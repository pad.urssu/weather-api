package com.gausslab.weatherapi.input.controller

import com.gausslab.weatherapi.input.exception.CantSaveCsvException
import com.gausslab.weatherapi.input.service.ForecastCsvReaderService
import com.gausslab.weatherapi.input.service.HistoricalCsvReaderService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/api/weather/csv")
class CsvReaderController(
        private val forecastCsvReaderService: ForecastCsvReaderService,
        private val historicalCsvReaderService: HistoricalCsvReaderService
) {
    @PostMapping
    @Operation(summary = "예보 CSV 파일 저장")
    @ApiResponses(
            ApiResponse(responseCode = "200", description = "CSV 파일이 성공적으로 저장됨"),
            ApiResponse(responseCode = "400", description = "CSV 파일의 컬럼 수가 일치하지 않거나 컬럼 이름과 엔티티 필드 이름이 일치하지 않음")
    )
    fun saveForecastCsvFile(
            @RequestBody csvFile: MultipartFile,
            @RequestParam("forecastedTime") forecastedTime: Long,
            @RequestParam("startTimestamp") startTimestamp: Long,
            @RequestParam("endTimestamp") endTimestamp: Long
    ): ResponseEntity<String> {
        val result = forecastCsvReaderService.saveCsvFile(csvFile, forecastedTime, startTimestamp, endTimestamp)
        return ResponseEntity.ok(result)
    }

    @PostMapping("/historical")
    @Operation(summary = "Historical CSV 파일 저장")
    @ApiResponses(
            ApiResponse(responseCode = "200", description = "CSV 파일이 성공적으로 저장됨"),
            ApiResponse(responseCode = "400", description = "CSV 파일의 컬럼 수가 일치하지 않거나 컬럼 이름과 엔티티 필드 이름이 일치하지 않음")
    )
    fun saveHistoricalCsvFile(
            @RequestBody csvFile: MultipartFile,
            @RequestParam("startTimestamp") startTimestamp: Long,
            @RequestParam("endTimestamp") endTimestamp: Long
    ): ResponseEntity<String> {
        val result = historicalCsvReaderService.saveCsvFile(csvFile, startTimestamp, endTimestamp)
        return ResponseEntity.ok(result)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    fun handleIllegalArgumentException(exception: IllegalArgumentException): ResponseEntity<String> {
        return ResponseEntity.badRequest().body(exception.message)
    }

    @ExceptionHandler(CantSaveCsvException::class)
    fun handleCantSaveCsvException(exception: CantSaveCsvException): ResponseEntity<String> {
        return ResponseEntity.badRequest().body(exception.message)
    }

}