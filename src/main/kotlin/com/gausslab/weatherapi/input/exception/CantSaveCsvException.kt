package com.gausslab.weatherapi.input.exception

class CantSaveCsvException : RuntimeException("Failed to save CSV file")