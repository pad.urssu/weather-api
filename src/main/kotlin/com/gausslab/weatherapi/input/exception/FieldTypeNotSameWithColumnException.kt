package com.gausslab.weatherapi.input.exception

class FieldTypeNotSameWithColumnException: RuntimeException("Field type not same with column type")