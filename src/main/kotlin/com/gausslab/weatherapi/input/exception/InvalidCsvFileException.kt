package com.gausslab.weatherapi.input.exception

class InvalidCsvFileException : RuntimeException("CSV file invalid")