package com.gausslab.weatherapi.input.model

class ForecastSaveInfo(
        startTimestamp: Long,
        endTimestamp: Long,
        val forecastedTime: Long,
        csvFile: String,
        csvFileName: String
) : SaveInfo(startTimestamp, endTimestamp, csvFile, csvFileName)