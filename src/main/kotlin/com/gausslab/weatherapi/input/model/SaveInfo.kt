package com.gausslab.weatherapi.input.model

import java.nio.file.Path
import java.nio.file.Paths

open class SaveInfo(
        val startTimestamp : Long,
        val endTimestamp : Long,
        val csvFile: String,
        val csvFileName: String
) {
    fun getCsvFilePath(): Path {
        return Paths.get(csvFile)
    }
}