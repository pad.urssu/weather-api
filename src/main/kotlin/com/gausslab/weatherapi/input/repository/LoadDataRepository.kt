package com.gausslab.weatherapi.input.repository

import com.gausslab.weatherapi.input.model.SaveInfo
import com.gausslab.weatherapi.input.model.ForecastSaveInfo
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KLogger
import mu.KotlinLogging
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.nio.file.Path
import java.util.*
import kotlin.coroutines.CoroutineContext
import kotlin.system.measureTimeMillis

@Repository
class LoadDataRepository(
        private val jdbcTemplate: JdbcTemplate,
        private val logger: KLogger = KotlinLogging.logger {}
) {
    private val scope = CoroutineScope(Dispatchers.IO)
    private val coroutineContext: CoroutineContext = scope.coroutineContext

    private val queue = WeatherDataInsertQueue()

    init {
        startProcessor()
    }

    private fun startProcessor() = scope.launch(coroutineContext) {
        processQueue()
    }

    fun addDataToQueue(saveInfo: SaveInfo) {
        queue.add(saveInfo)
    }

    private suspend fun processQueue() {
        while (coroutineContext.isActive) {
            queue.mutex.withLock {
                if (!queue.isEmpty()) {
                    val saveInfo = queue.poll()
                    saveInfo?.let {
                        val job = if (it is ForecastSaveInfo)
                            saveWeatherInfo(saveInfo.getCsvFilePath(), it.forecastedTime, saveInfo.csvFileName)
                        else
                            saveWeatherInfo(saveInfo.getCsvFilePath(), null, saveInfo.csvFileName)

                        job.join()
                    }
                }
            }
        }
    }


    private suspend fun saveWeatherInfo(file: Path, forecastedTime: Long?, csvFileName: String) = scope.launch {
        val loadDataSql = if (forecastedTime != null) {
            """
        LOAD DATA
        CONCURRENT
        LOCAL 
        INFILE '${file.toAbsolutePath().toString().replace("\\", "/")}'
        REPLACE
        INTO TABLE forecast_weather_info
        FIELDS TERMINATED BY ',' -- or any other delimiter
        LINES TERMINATED BY '\n' -- or any other line terminator
        IGNORE 1 LINES 
        (
            timestamp, latitude, longitude, temperature,
            apparent_temperature, soil_temperature, relative_humidity,
            dewpoint, pressure, surface_pressure, cloudcover,
            cloudcover_low, cloudcover_mid, cloudcover_high, wind_speed,
            wind_speed_high, wind_direction, wind_direction_high,
            wind_gust_speed, global_horizontal_irradiance, direct_irradiance,
            direct_normal_irradiance, diffuse_horizontal_irradiance,
            precipitation, snow, rain
        ) SET forecasted_time = '${forecastedTime}'; 
    """.trimIndent()
        } else {
            """
        LOAD DATA
        CONCURRENT
        LOCAL
        INFILE '${file.toAbsolutePath().toString().replace("\\", "/")}'
        REPLACE
        INTO TABLE historical_weather_info
        FIELDS TERMINATED BY ',' -- or any other delimiter
        LINES TERMINATED BY '\n' -- or any other line terminator
        IGNORE 1 LINES
        (
            timestamp, latitude, longitude, temperature,
            apparent_temperature, soil_temperature, relative_humidity,
            dewpoint, pressure, surface_pressure, cloudcover,
            cloudcover_low, cloudcover_mid, cloudcover_high, wind_speed,
            wind_speed_high, wind_direction, wind_direction_high,
            wind_gust_speed, global_horizontal_irradiance, direct_irradiance,
            direct_normal_irradiance, diffuse_horizontal_irradiance,
            precipitation, snow, rain
        );
        """.trimIndent()
        }
        //소요 시간
        val elapsedTime = measureTimeMillis {
            jdbcTemplate.execute(loadDataSql)
        }
        logger.info { "saved : $csvFileName in $elapsedTime" }
        //큐에 몇개 남았는지 로그
        logger.info { "queue size : ${queue.size()}" }
    }
}

class WeatherDataInsertQueue(
        private val logger: KLogger = KotlinLogging.logger {}
) {
    val mutex = Mutex(false)

    private val saveInfoQueues: PriorityQueue<SaveInfo> = PriorityQueue(
            //큐에 추가할 때 ForecastSaveInfo / SaveInfo 구분 후, 시작 시간으로 우선순위 결정
            compareBy<SaveInfo>({ it !is ForecastSaveInfo }, { it.startTimestamp })
    )

    fun add(toAdd: SaveInfo) {
        this.saveInfoQueues.add(toAdd)
    }


    fun poll(): SaveInfo? {
        return if (this.saveInfoQueues.isNotEmpty()) {
            this.saveInfoQueues.poll()
        } else {
            // 아무것도 큐에 없으면 아무것도 안함
            null
        }
    }

    fun isEmpty(): Boolean {
        return this.saveInfoQueues.isEmpty()
    }

    fun size(): Int {
        return this.saveInfoQueues.size
    }
}


