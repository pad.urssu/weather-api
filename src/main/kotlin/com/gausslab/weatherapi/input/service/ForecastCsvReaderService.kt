package com.gausslab.weatherapi.input.service

import com.gausslab.weatherapi.input.repository.LoadDataRepository
import com.gausslab.weatherapi.input.model.ForecastSaveInfo
import mu.KLogger
import mu.KotlinLogging
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class ForecastCsvReaderService(
        private val logger: KLogger = KotlinLogging.logger {},
        private val loadDataRepository: LoadDataRepository
) {
    fun saveCsvFile(csvFile: MultipartFile, forecastedTime: Long, startTimestamp: Long, endTimestamp: Long): String {
        logger.info { "${csvFile.originalFilename} : Saving Started" }
        //CsvValidator.validateCsvFile(csvFile, ForecastWeatherInfo::class.java, listOf("timestamp"))
        val tempFileString = convertCsvIntoTempFile(csvFile)
        loadDataRepository.addDataToQueue(
                ForecastSaveInfo(
                        startTimestamp = startTimestamp,
                        endTimestamp = endTimestamp,
                        forecastedTime = forecastedTime,
                        csvFile = tempFileString,
                        csvFileName = csvFile.originalFilename!!
                )
        )

        return "saving ${csvFile.originalFilename}-${forecastedTime}-${startTimestamp}-${endTimestamp} has been started"
    }

    fun convertCsvIntoTempFile(csvFile: MultipartFile): String {
        val tempFile = kotlin.io.path.createTempFile()
        csvFile.transferTo(tempFile)
        return tempFile.toString()
    }
}
