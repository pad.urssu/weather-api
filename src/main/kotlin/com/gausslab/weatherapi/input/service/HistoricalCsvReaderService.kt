package com.gausslab.weatherapi.input.service

import com.gausslab.weatherapi.input.model.SaveInfo
import com.gausslab.weatherapi.input.repository.LoadDataRepository
import mu.KLogger
import mu.KotlinLogging
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class HistoricalCsvReaderService(
        private val logger: KLogger = KotlinLogging.logger {},
        private val loadDataRepository: LoadDataRepository
) {
    fun saveCsvFile(csvFile: MultipartFile, startTimestamp: Long, endTimestamp: Long): String {
        logger.info { "${csvFile} : Saving Started" }
        val tempFileString = convertCsvIntoTempFile(csvFile)
        loadDataRepository.addDataToQueue(
                SaveInfo(
                        startTimestamp = startTimestamp,
                        endTimestamp = endTimestamp,
                        csvFile = tempFileString,
                        csvFileName = csvFile.originalFilename!!
                )
        )

        return "saving ${csvFile}-${startTimestamp}-${endTimestamp} has been started"
    }

    fun convertCsvIntoTempFile(csvFile: MultipartFile): String {
        val tempFile = kotlin.io.path.createTempFile()
        csvFile.transferTo(tempFile)
        return tempFile.toString()
    }

}