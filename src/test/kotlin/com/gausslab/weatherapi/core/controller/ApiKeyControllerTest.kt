package com.gausslab.weatherapi.core.controller

import com.gausslab.weatherapi.core.entity.ApiKey
import com.gausslab.weatherapi.core.entity.User
import com.gausslab.weatherapi.core.enums.ApiKeyStatus
import com.gausslab.weatherapi.core.service.ApiKeyService
import com.gausslab.weatherapi.security.WithMockCustomUser
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@WebMvcTest(ApiKeyController::class)
class ApiKeyControllerTest {

    @MockBean
    private lateinit var apiKeyService: ApiKeyService

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var context: WebApplicationContext

    @BeforeEach
    fun setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply<DefaultMockMvcBuilder?>(springSecurity())
                .build()
    }

    @Nested
    @DisplayName("api key 관리")
    inner class ApiKeyManagement {
        @Test
        @DisplayName("성공 / api 키 리턴")
        @WithMockCustomUser(username = "user@gmail.com")
        fun `generate valid API key`() {
            val email = "user@gmail.com"
            val user = User(displayName = "user", userEmail = email, userPassword = "password")
            val apiKey = ApiKey(user = user, keyHash = "jwtjwtjwtjwt", apiKeyStatus = ApiKeyStatus.ACTIVE)

            Mockito.`when`(apiKeyService.generateAndAssignKey(email)).thenReturn(apiKey)

            mockMvc.perform(post("/api/keys/generate")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$.key").value(apiKey.keyHash))
                    .andExpect(jsonPath("$.status").value(apiKey.apiKeyStatus.toString()))
                    .andExpect(jsonPath("$.message").value("Generated"))
        }

        @Test
        @DisplayName("성공 / 삭제된 api 키 리턴")
        @WithMockCustomUser(username = "user@gmail.com")
        fun `delete API key successfully`() {
            val email = "user@gmail.com"
            val keyHash = "jwtjwtjwtjwt"
            val deletedApiKey = ApiKey(user = User(displayName = "user", userEmail = email, userPassword = "password"), keyHash = keyHash, apiKeyStatus = ApiKeyStatus.INACTIVE)

            Mockito.`when`(apiKeyService.deleteKey(email, keyHash)).thenReturn(deletedApiKey)

            mockMvc.perform(delete("/api/keys/delete")
                    .with(csrf())
                    .param("key", keyHash)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$.key").value(keyHash))
                    .andExpect(jsonPath("$.status").value(deletedApiKey.apiKeyStatus.toString()))
                    .andExpect(jsonPath("$.message").value("API key deleted successfully"))
        }
    }
}
