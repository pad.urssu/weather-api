package com.gausslab.weatherapi.core.service

import com.gausslab.weatherapi.core.entity.ApiKey
import com.gausslab.weatherapi.core.entity.ApiKeyUsage
import com.gausslab.weatherapi.core.entity.User
import com.gausslab.weatherapi.core.enums.ApiKeyStatus
import com.gausslab.weatherapi.core.repository.ApiKeyRepository
import com.gausslab.weatherapi.core.repository.ApiKeyUsageRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.ObjectProvider
import org.springframework.data.domain.Sort
import java.util.*

class ApiKeyServiceTest {
    private val mockRepository: ApiKeyRepository = mockk(relaxed = true)
    private val mockApiKeyUsageRepository: ApiKeyUsageRepository = mockk(relaxed = true)
    private val mockUserService: UserService = mockk(relaxed = true)
    private val userServiceProvider: ObjectProvider<UserService> = mockk {
        every { getObject() } returns mockUserService
    }
    private val service = ApiKeyService(mockRepository, mockApiKeyUsageRepository, userServiceProvider)

    val email = "user@example.com"
    val displayName = "user"
    val password = "password"
    val key = "key_hash"
    val keyHash = "jwtjwtjwtjwtjwtjwtjwtjwtjwtjwtjwtjwtjwtjwtjwtjwt"

    @Nested
    @DisplayName("api key 관리")
    inner class ApiKeyManagement {
        @Test
        @DisplayName("성공 / api 키 생성")
        fun `generate valid api key`() {
            val user = User(displayName = displayName, userEmail = email, userPassword = password)
            val uuid = UUID.randomUUID().toString()
            val apiKey = ApiKey(keyHash = uuid, user = user)

            every { mockUserService.getUserFromEmail(email) } returns user
            every { mockRepository.save(any<ApiKey>()) } returns apiKey

            val result = service.generateAndAssignKey(email)

            assertEquals(apiKey, result)
            verify { mockRepository.save(any<ApiKey>()) }
        }

        @Test
        @DisplayName("성공 / api 키 삭제")
        fun `delete valid api key`() {
            val user = User(displayName = displayName, userEmail = email, userPassword = password)
            val uuid = UUID.randomUUID().toString()
            val apiKey = ApiKey(keyHash = uuid, user = user)

            every { mockUserService.getUserFromEmail(email) } returns user
            every { mockRepository.findByKeyHashAndUser(uuid, user) } returns apiKey

            val result = service.deleteKey(email, uuid)

            assertEquals(apiKey, result)
            verify { mockRepository.delete(apiKey) }
        }

        @Test
        @DisplayName("성공 / api 키 목록 조회")
        fun `get api key list`() {
            val user = User(displayName = displayName, userEmail = email, userPassword = password)
            val uuid = UUID.randomUUID().toString()
            val apiKey = ApiKey(keyHash = uuid, user = user)

            every { mockUserService.getUserFromEmail(email) } returns user
            every { mockRepository.findAllByUser(user) } returns listOf(apiKey)

            val result = service.getKeys(email)

            assertEquals(listOf(apiKey), result)
        }

        @Test
        @DisplayName("성공 / api 키 유효성 검증")
        fun `validate api key`() {
            every { mockRepository.existsByKeyHash(key) } returns true

            val result = service.validateKey(key)

            assertEquals(true, result)
        }

        @Test
        @DisplayName("성공 / api 키 사용 증가")
        fun `increase api key usage`() {
            val apiKey = ApiKey(keyHash = key, user = User(displayName = displayName, userEmail = email, userPassword = password))

            service.increaseUsage(apiKey)

            verify { mockApiKeyUsageRepository.save(ApiKeyUsage(apiKey = apiKey)) }
        }

        @Test
        @DisplayName("성공 / api 키 가져오기")
        fun `get api key`() {
            val apiKey = ApiKey(keyHash = key, user = User(displayName = displayName, userEmail = email, userPassword = password))

            every { mockRepository.findByKeyHash(key) } returns apiKey

            val result = service.getKey(key)

            assertEquals(apiKey, result)
        }

        @Test
        @DisplayName("성공 / api 키 사용 내역 가져오기")
        fun `get api key usage`() {
            val apiKey = ApiKey(keyHash = key, user = User(displayName = displayName, userEmail = email, userPassword = password))
            val usageList = listOf(ApiKeyUsage(apiKey = apiKey))

            every { mockRepository.findByKeyHash(key) } returns apiKey
            val sort = Sort.by(Sort.Direction.DESC, "usageDate")
            every { mockApiKeyUsageRepository.findAllByApiKey(apiKey, sort) } returns usageList

            val result = service.getKeyUsage(key)

            assertEquals(usageList, result)
        }

        @Test
        @DisplayName("성공 / api 키 비활성화")
        fun `set api key status to inactive`() {
            val user = User(displayName = displayName, userEmail = email, userPassword = password)
            val uuid = UUID.randomUUID().toString()
            val apiKey = ApiKey(keyHash = uuid, user = user)

            every { mockUserService.getUserFromEmail(email) } returns user
            every { mockRepository.save(any<ApiKey>()) } returns apiKey

            val generatedApiKey = service.generateAndAssignKey(email)

            every { mockRepository.findByKeyHash(generatedApiKey.keyHash) } returns apiKey
            apiKey.apiKeyStatus = ApiKeyStatus.INACTIVE
            every { mockRepository.save(apiKey) } returns apiKey

            val result = service.setApiKeyStatusToInactive(uuid)

            assertEquals(ApiKeyStatus.INACTIVE, result?.apiKeyStatus)
            verify { mockRepository.save(apiKey) }
        }
    }
}
