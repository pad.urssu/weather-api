package com.gausslab.weatherapi.datafetch.controller

import com.gausslab.weatherapi.datafetch.service.WeatherInfoService
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class WeatherInfoGraphQLControllerTest {

    private val weatherInfoService = mockk<WeatherInfoService>()
    private val weatherInfoGraphQLController = WeatherInfoGraphQLController(weatherInfoService)

//    @Test
//    fun `controllerTest - 시작timestamp랑 끝timestamp를 알고, latitude랑 longitude를 알 때`() {
//        val startDate = "1674288000"
//        val endDate = "1674291600"
//        val latitude = 33f
//        val longitude = 126f
//
//        val expectedWeatherInfoList = listOf(
//                WeatherInfo(timestamp = 1674288000L, temperature = "22.7", latitude = 33f, longitude = 126f),
//                WeatherInfo(timestamp = 1674291600L, temperature = "23.7", latitude = 33f, longitude = 126f)
//        )
//
//        every {
//            weatherInfoService.getWeatherInfo(startDate, endDate, latitude, longitude, null)
//        } returns expectedWeatherInfoList
//
//        val result: List<WeatherInfo> = weatherInfoController.weatherInfo(startDate, endDate, latitude, longitude)
//
//        assertEquals(expectedWeatherInfoList, result)
//    }

    @Test
    fun `handleNumberFormatException - NumberFormatException이 발생하면 400 Bad Request랑 메시지 반환`() {
        val exception = NumberFormatException("Invalid number format")

        val response: ResponseEntity<String> = weatherInfoGraphQLController.handleNumberFormatException(exception)

        assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
        assertEquals("Invalid number format", response.body)
    }

    @Test
    fun `handleRuntimeException - RuntimeException이 발생하면 400 Bad Request랑 메시지 반환`() {
        val exception = RuntimeException("Runtime Exception")

        val response: ResponseEntity<String> = weatherInfoGraphQLController.handleRuntimeException(exception)

        assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
        assertEquals("Runtime Exception", response.body)
    }
}
