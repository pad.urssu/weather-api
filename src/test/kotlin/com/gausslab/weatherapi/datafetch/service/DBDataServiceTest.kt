package com.gausslab.weatherapi.datafetch.service

import com.gausslab.weatherapi.core.repository.ForecastWeatherInfoRepository
import com.gausslab.weatherapi.core.repository.HistoricalWeatherInfoRepository
import com.gausslab.weatherapi.datafetch.dto.CoordinatesRange
import com.gausslab.weatherapi.datafetch.dto.TimestampRange
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DBDataServiceTest {

    private val forecastWeatherInfoRepository = mockk<ForecastWeatherInfoRepository>()
    private val historicalWeatherInfoRepository = mockk<HistoricalWeatherInfoRepository>()
    private val dbDataService = DBDataService(forecastWeatherInfoRepository, historicalWeatherInfoRepository)

    @Test
    fun `어플리케이션이 실행되면 ForecastDB에서 latitude,longitude min,max값 읽어오기`() {
        val mockCoordinatesRange = CoordinatesRange(34.0f, 37.0f, 126.0f, 129.0f)
        every { dbDataService.retrieveForecastMinMaxCoordinates() } returns mockCoordinatesRange

        val result = dbDataService.retrieveForecastMinMaxCoordinates()

        assertEquals(mockCoordinatesRange, result)
    }

    @Test
    fun `어플리케이션이 실행되면 ForecastDB에서 timestamp min,max값 읽어오기`() {
        val mockTimestampRange = TimestampRange(1688256100L, 1693612900L)
        every { dbDataService.retrieveForecastMinMaxTimestamp() } returns mockTimestampRange

        val result = dbDataService.retrieveForecastMinMaxTimestamp()

        assertEquals(mockTimestampRange, result)
    }

    @Test
    fun `어플리케이션이 실행되면 HistoricalDB에서 latitude,longitude min,max값 읽어오기`() {
        val mockCoordinatesRange = CoordinatesRange(34.0f, 37.0f, 126.0f, 129.0f)
        every { dbDataService.retrieveHistoricalMinMaxCoordinates() } returns mockCoordinatesRange

        val result = dbDataService.retrieveHistoricalMinMaxCoordinates()

        assertEquals(mockCoordinatesRange, result)
    }

    @Test
    fun `어플리케이션이 실행되면 HistoricalDB에서 timestamp min,max값 읽어오기`() {
        val mockTimestampRange = TimestampRange(1688256100L, 1693612900L)
        every { dbDataService.retrieveHistoricalMinMaxTimestamp() } returns mockTimestampRange

        val result = dbDataService.retrieveHistoricalMinMaxTimestamp()

        assertEquals(mockTimestampRange, result)
    }
}