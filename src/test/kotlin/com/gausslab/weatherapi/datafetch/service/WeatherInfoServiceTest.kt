package com.gausslab.weatherapi.datafetch.service

import com.gausslab.weatherapi.CLOSESTPOINT_SEARCH_RADIUS
import com.gausslab.weatherapi.ONE_DAY_IN_SECONDS
import com.gausslab.weatherapi.core.entity.ForecastWeatherInfo
import com.gausslab.weatherapi.core.entity.HistoricalWeatherInfo
import com.gausslab.weatherapi.core.repository.ForecastWeatherInfoRepository

import com.gausslab.weatherapi.core.repository.HistoricalWeatherInfoRepository

import com.gausslab.weatherapi.datafetch.dto.WeatherResult
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class WeatherInfoServiceTest {

    private val forecastWeatherInfoRepository = mockk<ForecastWeatherInfoRepository>()
    private val historicalWeatherInfoRepository = mockk<HistoricalWeatherInfoRepository>()
    private val weatherInfoService = WeatherInfoService(forecastWeatherInfoRepository, historicalWeatherInfoRepository)

    @Test
    fun `date랑 forecastedTime을 알고, latitude랑 longitude를 알 때, 해당하는 위치에 가장 가까운 그 날짜시간의 가장 가까운 이전시간 값을 가져와`() {
        val date = "1688256000"
        val forecastedTime = "01:00"
        val latitude = 33f
        val longitude = 126f

        val expectedWeatherInfoList = listOf(
            ForecastWeatherInfo(
                timestamp = 1688256000L,
                forecastedTime = 1688259600L,
                temperature = "22.7",
                latitude = 33f,
                longitude = 126f
            )
        )

        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(
                date.toLong() + ONE_DAY_IN_SECONDS,
                date.toLong() + ONE_DAY_IN_SECONDS + ONE_DAY_IN_SECONDS,
                latitude - CLOSESTPOINT_SEARCH_RADIUS,
                latitude + CLOSESTPOINT_SEARCH_RADIUS,
                longitude - CLOSESTPOINT_SEARCH_RADIUS,
                longitude + CLOSESTPOINT_SEARCH_RADIUS
            )
        } returns expectedWeatherInfoList // 가장 가까운 WeatherInfo 반환

        every {
            forecastWeatherInfoRepository.findRelevantWeatherInfoFromRequestedSearchTime(
                latitude,
                longitude,
                date.toLong() + ONE_DAY_IN_SECONDS,
                date.toLong() + ONE_DAY_IN_SECONDS + ONE_DAY_IN_SECONDS,
                1688259600L
            )
        } returns expectedWeatherInfoList

        val result = weatherInfoService.getWeatherInfo(date, forecastedTime, latitude, longitude, null)

        assertEquals(expectedWeatherInfoList, result.flatMap { weatherResult ->
            when (weatherResult) {
                is WeatherResult.Forecast -> weatherResult.info
                else -> throw IllegalArgumentException("Received unexpected WeatherResult type: Want Forecast Weather")
            }
        })
    }

    @Test
    fun `시작 설정 범위 이내에 정보가 없으면 numExpansions만큼 돌려서 정보를 찾아서 반환`() {
        val date = "1688256000"
        val forecastedTime = "01:00"
        val latitude = 33f
        val longitude = 126f

        val expectedWeatherInfoList = listOf(
            ForecastWeatherInfo(temperature = "22.7", latitude = 33f, longitude = 126f)
        )

        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(
                any(), any(),
                33f - CLOSESTPOINT_SEARCH_RADIUS,
                33f + CLOSESTPOINT_SEARCH_RADIUS,
                126f - CLOSESTPOINT_SEARCH_RADIUS,
                126f + CLOSESTPOINT_SEARCH_RADIUS
            )
        } returns emptyList()

        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(
                any(), any(),
                33f - CLOSESTPOINT_SEARCH_RADIUS * 2,
                33f + CLOSESTPOINT_SEARCH_RADIUS * 2,
                126f - CLOSESTPOINT_SEARCH_RADIUS * 2,
                126f + CLOSESTPOINT_SEARCH_RADIUS * 2
            )
        } returns expectedWeatherInfoList

        every {
            forecastWeatherInfoRepository.findRelevantWeatherInfoFromRequestedSearchTime(any(), any(), any(), any(), any())
        } returns expectedWeatherInfoList

        val result = weatherInfoService.getWeatherInfo(date, forecastedTime, latitude, longitude, null)

        assertEquals(expectedWeatherInfoList, result.flatMap { weatherResult ->
            when (weatherResult) {
                is WeatherResult.Forecast -> weatherResult.info
                else -> throw IllegalArgumentException("Received unexpected WeatherResult type: Want Forecast Weather")
            }
        })
    }


    @Test
    fun `date가 null이고 forecastedTime이 null일 때 내일의 데이터를 반환`() {
        val calendar = Calendar.getInstance()

        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrowEpochTimeInSeconds = calendar.timeInMillis / 1000

        val expectedWeatherInfoList = listOf(
            ForecastWeatherInfo(timestamp = tomorrowEpochTimeInSeconds, temperature = "22.7", latitude = 33f, longitude = 126f)
        )

        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(
                any(), any(), any(), any(), any(), any()
            )
        } returns expectedWeatherInfoList

        every {
            forecastWeatherInfoRepository.findRelevantWeatherInfoFromRequestedSearchTime(
                any(), any(), any(), any(), any()
            )
        } returns expectedWeatherInfoList

        val result = weatherInfoService.getWeatherInfo(null, null, 33f, 126f, null)

        assertEquals(expectedWeatherInfoList, result.flatMap { weatherResult ->
            when (weatherResult) {
                is WeatherResult.Forecast -> weatherResult.info
                else -> throw IllegalArgumentException("Received unexpected WeatherResult type: Want Forecast Weather")
            }
        })
    }

    @Test
    fun `startDate랑 endDate가 있고 latitude랑 longitude를 알 때, 해당하는 위치에 가장 가까운 그 날짜시간의 과거 값을 가져와`() {
        val startDate = "1688256000"
        val latitude = 33f
        val longitude = 126f
        val endDate = "1688259600"

        val expectedHistoricalWeatherInfoList = listOf(
            HistoricalWeatherInfo(timestamp = 1688256000L, temperature = "22.7", latitude = 33f, longitude = 126f),
            HistoricalWeatherInfo(timestamp = 1688259600L, temperature = "22.7", latitude = 33f, longitude = 126f)
        )
        val expectedWeatherInfoList = listOf(
            ForecastWeatherInfo(timestamp = 1688256000L, temperature = "22.7", latitude = 33f, longitude = 126f)
        )

        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(
                any(),
                any(),
                33f - CLOSESTPOINT_SEARCH_RADIUS,
                33f + CLOSESTPOINT_SEARCH_RADIUS,
                126f - CLOSESTPOINT_SEARCH_RADIUS,
                126f + CLOSESTPOINT_SEARCH_RADIUS
            )
        } returns expectedWeatherInfoList

        every {
            historicalWeatherInfoRepository.findAllByLatitudeEqualsAndLongitudeEqualsAndTimestampBetween(
                latitude,
                longitude,
                any(),
                any(),
            )
        } returns expectedHistoricalWeatherInfoList

        val result = weatherInfoService.getWeatherInfo(startDate, null, latitude, longitude, endDate)

        assertEquals(expectedHistoricalWeatherInfoList, result.flatMap { weatherResult ->
            when (weatherResult) {
                is WeatherResult.Historical -> weatherResult.info
                else -> throw IllegalArgumentException("Received unexpected WeatherResult type: Want Forecast Weather")
            }
        })
    }

    @Test
    fun `numExpansions만큼 돌려서 정보가 없으면 IllegalArgumentException`() {
        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(any(), any(), any(), any(), any(), any())
        } returns emptyList()

        assertThrows<IllegalArgumentException> {
            weatherInfoService.getWeatherInfo("1674288000", "14:00", 33f, 126f, null)
        }
    }


    @Test
    fun `날짜에 문자가 들어가 있으면 IllegalArgumentException`() {
        val date = "1674288000asd"
        val forecastedTime = "14:00"
        val latitude = 33f
        val longitude = 126f

        assertThrows<IllegalArgumentException> {
            weatherInfoService.getWeatherInfo(date, forecastedTime, latitude, longitude, null)
        }
    }


    @Test
    fun `latitude랑 longitude가 유효하지 않을때 IllegalArgumentException`() {
        val date = "1674288000"
        val forecastedTime = "14:00"
        val latitude = 34234f
        val longitude = 34423f

        assertThrows<IllegalArgumentException> {
            weatherInfoService.getWeatherInfo(date, forecastedTime, latitude, longitude, null)
            weatherInfoService.getWeatherInfo(null, null, latitude, longitude, null)
        }
    }

    @Test
    fun `시작날짜가 유효하지 않은 경우 IllegalArgumentException`() {
        val date = "invalid"
        val forecastedTime = "14:00"
        val latitude = 33f
        val longitude = 126f

        assertThrows<IllegalArgumentException> {
            weatherInfoService.getWeatherInfo(date, forecastedTime, latitude, longitude, null)
        }
    }

    @Test
    fun `weatherInfo가 비어있으면 IllegalArgumentException`() {
        val date = "1688256000"
        val forecastedTime = "01:00"
        val latitude = 33f
        val longitude = 126f

        val expectedWeatherInfoList = listOf(
            ForecastWeatherInfo(temperature = "22.7", latitude = 33f, longitude = 126f)
        )

        every {
            forecastWeatherInfoRepository.findWithinBoundingBox(
                any(), any(),
                33f - CLOSESTPOINT_SEARCH_RADIUS,
                33f + CLOSESTPOINT_SEARCH_RADIUS,
                126f - CLOSESTPOINT_SEARCH_RADIUS,
                126f + CLOSESTPOINT_SEARCH_RADIUS
            )
        } returns expectedWeatherInfoList

        every {
            forecastWeatherInfoRepository.findRelevantWeatherInfoFromRequestedSearchTime(any(), any(), any(), any(), any())
        } returns emptyList()

        assertThrows<IllegalArgumentException> {
            weatherInfoService.getWeatherInfo(date, forecastedTime, latitude, longitude, null)
        }
    }
}