package com.gausslab.weatherapi.input

import com.gausslab.weatherapi.core.entity.ForecastWeatherInfo
import com.gausslab.weatherapi.core.util.CsvValidator
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileInputStream

class CsvValidatorTest {

    @Nested
    @DisplayName("csv 저장")
    inner class ValidateCsv {
        @Test
        @DisplayName("성공 / 정상 파일로 정상 작동되는 경우")
        fun `test validateCsvFile with valid csv file`() {
            val multipartFile: MultipartFile = mockk<MockMultipartFile>()

            val csvFile = File("src/test/resources/2023-07-01_6km_spaced_1.csv")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)
            assertDoesNotThrow {
                CsvValidator.validateCsvFile(multipartFile, ForecastWeatherInfo::class.java, listOf("timestamp"))
            }

        }

        @Test
        @DisplayName("실패 / IllegalArgumentException()리턴, 잘못된 확장자로 실패")
        fun `test validateCsvFile with invalid file extension`() {
            val multipartFile: MultipartFile = mockk<MockMultipartFile>()

            val txtFile = File("src/test/resources/invalidExtension.txt")
            every { multipartFile.inputStream } returns FileInputStream(txtFile)

            assertThrows<Exception> {
                CsvValidator.validateCsvFile(multipartFile, ForecastWeatherInfo::class.java, listOf("timestamp"))
            }
        }

        @Test
        @DisplayName("실패 / IllegalArgumentException()리턴, 컬럼 수 != 엔티티 필드 수라서 실패")
        fun `test validateCsvFile with mismatched columns`() {
            val multipartFile: MultipartFile = mockk<MockMultipartFile>()

            val csvFile = File("src/test/resources/numberOfColumnsNotMatchTest.csv")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            assertThrows<Exception> {
                CsvValidator.validateCsvFile(multipartFile, ForecastWeatherInfo::class.java, listOf("timestamp"))
            }
        }

        @Test
        @DisplayName("실패 / IllegalArgumentException()리턴, 엔티티의 데이터 타입과 csv의 데이터 타입이 맞지 않아 실패")
        fun `test validateCsvFile with mismatched types`() {
            val multipartFile: MultipartFile = mockk<MockMultipartFile>()

            val csvFile = File("src/test/resources/invalidDataFormat.csv")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            assertThrows<Exception> {
                CsvValidator.validateCsvFile(multipartFile, ForecastWeatherInfo::class.java, listOf("timestamp"))
            }
        }

        @Test
        @DisplayName("실패 / IllegalArgumentException()리턴, 변환된 내용을 DB에 저장하는데 실패")
        fun `test validateCsvFile with failed to save to db`() {
            val multipartFile: MultipartFile = mockk<MockMultipartFile>()

            val csvFile = File("src/test/resources/valid.csv")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            assertThrows<Exception> {
                CsvValidator.validateCsvFile(multipartFile, ForecastWeatherInfo::class.java, listOf("timestamp"))
            }
        }
    }
}
