package com.gausslab.weatherapi.input.controller

import com.gausslab.weatherapi.input.service.ForecastCsvReaderService
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.io.File
import java.io.FileInputStream

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class CsvReaderControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var csvReaderService: ForecastCsvReaderService

    @Nested
    @DisplayName("CSV 파일 저장")
    inner class SaveCsvFile {


        @Test
        @DisplayName("성공 / String 리턴, 정상 파일로 정상 작동되는 경우")
        fun `with valid csv file`() {
            //예제 파라미터 생성
            val forecastedTime = 1000L
            val startTimestamp = 1010L
            val endTimestamp = 1020L
            //resource에서 가져온 2023-07-01_6km_spaced_1.csv 파일
            val csvFile = File("src/test/resources/2023-07-01_6km_spaced_1.csv")
            val multipartFile: MockMultipartFile = mockk(relaxed = true)
            every { multipartFile.inputStream } returns FileInputStream(csvFile)
            //mocking
            `when`(csvReaderService.saveCsvFile(multipartFile,forecastedTime,startTimestamp,endTimestamp)).thenReturn("success")

            //나머지 테스트 코드 수행
            mockMvc.perform(
                    multipart("/api/weather/csv")
                            .file("csvFile", multipartFile.bytes)
                            .param("forecastedTime", forecastedTime.toString())
                            .param("startTimestamp", startTimestamp.toString())
                            .param("endTimestamp", endTimestamp.toString())
            )
                    .andExpect(status().isOk)
                    .andReturn()
        }
    }
}
