package com.gausslab.weatherapi.input.repository

import com.gausslab.weatherapi.input.model.ForecastSaveInfo
import com.gausslab.weatherapi.input.model.SaveInfo
import com.gausslab.weatherapi.input.service.ForecastCsvReaderService
import com.gausslab.weatherapi.input.service.HistoricalCsvReaderService
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.io.File
import java.io.FileInputStream

@ExtendWith(SpringExtension::class)
@SpringBootTest
class WeatherDataInsertQueueTest(
        @Autowired private val forecastCsvReaderService: ForecastCsvReaderService,
        @Autowired private val historicalCsvReaderService: HistoricalCsvReaderService
) {
    private val weatherDataInsertQueue = WeatherDataInsertQueue()

    @Nested
    @DisplayName("큐 데이터 추가")
    inner class QueueTest {
        @Test
        @DisplayName("성공 / 큐 입출력 정상 작동 확인")
        fun `test add and poll functionality`() {
            val csvFile = File("src/test/resources/2023-07-01_6km_spaced_1.csv")
            val multipartFile: MockMultipartFile = mockk(relaxed = true)
            every { multipartFile.inputStream } returns FileInputStream(csvFile)


            val tempFile = forecastCsvReaderService.convertCsvIntoTempFile(multipartFile)

            val saveInfo1 = ForecastSaveInfo(1000, 1010, 999, tempFile, "2023-07-01_6km_spaced_1.csv")
            val saveInfo2 = ForecastSaveInfo(1010, 1020, 999, tempFile, "2023-07-01_6km_spaced_1.csv")


            weatherDataInsertQueue.add(saveInfo1)
            weatherDataInsertQueue.add(saveInfo2)
            assertFalse(weatherDataInsertQueue.isEmpty())

            // poll 작동하는지 확인
            assertEquals(saveInfo1, weatherDataInsertQueue.poll())
            assertEquals(saveInfo2, weatherDataInsertQueue.poll())
            assertNull(weatherDataInsertQueue.poll())
            assertTrue(weatherDataInsertQueue.isEmpty())
        }

        @Test
        @DisplayName("성공 / 우선순위 정상 작동 확인")
        fun `test priority order in queue`() {
            val csvFile = File("src/test/resources/2023-07-01_6km_spaced_1.csv")
            val multipartFile: MockMultipartFile = mockk(relaxed = true)
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            val tempFile = forecastCsvReaderService.convertCsvIntoTempFile(multipartFile)

            val saveInfo1 = ForecastSaveInfo(1000, 1010, 999, tempFile, "2023-07-01_6km_spaced_1.csv")
            val saveInfo2 = SaveInfo(1010, 1020, tempFile, "2023-07-01_6km_spaced_1.csv")
            val saveInfo3 = ForecastSaveInfo(1020, 1020, 999, tempFile, "2023-07-01_6km_spaced_1.csv")
            val saveInfo4 = ForecastSaveInfo(1030, 1020, 999, tempFile, "2023-07-01_6km_spaced_1.csv")

            weatherDataInsertQueue.add(saveInfo4)
            weatherDataInsertQueue.add(saveInfo2)
            weatherDataInsertQueue.add(saveInfo1)
            weatherDataInsertQueue.add(saveInfo3)

            // 우선순위 검증
            assertEquals(saveInfo1, weatherDataInsertQueue.poll())
            assertEquals(saveInfo3, weatherDataInsertQueue.poll())
            assertEquals(saveInfo4, weatherDataInsertQueue.poll())
            assertEquals(saveInfo2, weatherDataInsertQueue.poll())
        }
    }
}