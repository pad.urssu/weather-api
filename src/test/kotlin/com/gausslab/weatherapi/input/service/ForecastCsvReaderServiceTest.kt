package com.gausslab.weatherapi.input.service

import com.gausslab.weatherapi.input.repository.LoadDataRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.mock.web.MockMultipartFile
import java.io.File
import java.io.FileInputStream

class ForecastCsvReaderServiceTest {

    private val mockRepository: LoadDataRepository = mockk(relaxed = true)
    private val service = ForecastCsvReaderService(loadDataRepository = mockRepository)

    @Nested
    @DisplayName("csv 저장")
    inner class SaveCsvFile {
        @Test
        @DisplayName("성공 / String 리턴, 정상 파일로 정상 작동되는 경우")
        fun `with valid csv file`() {
            val multipartFile: MockMultipartFile = mockk(relaxed = true)
            val csvFile = File("src/test/resources/2023-07-01_6km_spaced_1.csv")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            val response = service.saveCsvFile(multipartFile, 900, 1000, 1010)

            assertEquals("saving -900-1000-1010 has been started", response)
        }

        @Test
        @DisplayName("실패 / IllegalArgumentException 리턴, 잘못된 확장자로 실패")
        fun `with invalid csv file`() {
            val multipartFile: MockMultipartFile = mockk(relaxed = true)
            val csvFile = File("src/test/resources/invalidExtension.txt")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            assertThrows<IllegalArgumentException> {
                service.saveCsvFile(multipartFile, 900, 1000, 1010)
            }
        }

        @Test
        @DisplayName("실패 / IllegalArgumentException리턴 DB 오류로 실패")
        fun `with database save error`() {
            val multipartFile: MockMultipartFile = mockk(relaxed = true)
            val csvFile = File("src/test/resources/valid.csv")
            every { multipartFile.inputStream } returns FileInputStream(csvFile)

            assertThrows<IllegalArgumentException> {
                service.saveCsvFile(multipartFile, 900, 1000, 1010)
            }
        }
    }

}
