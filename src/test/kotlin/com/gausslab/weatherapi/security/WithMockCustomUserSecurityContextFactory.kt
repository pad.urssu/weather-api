package com.gausslab.weatherapi.security

import com.gausslab.weatherapi.core.security.UserPrincipal
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithSecurityContextFactory

class WithMockCustomUserSecurityContextFactory : WithSecurityContextFactory<WithMockCustomUser> {
    override fun createSecurityContext(customUser: WithMockCustomUser): SecurityContext {
        val principal = UserPrincipal(customUser.username)
        // 필요한 권한을 추가하세요.
        val authorities = listOf(SimpleGrantedAuthority("ROLE_USER"))
        val auth: Authentication = UsernamePasswordAuthenticationToken(principal, null, authorities)
        val context = SecurityContextHolder.createEmptyContext()
        context.authentication = auth
        return context
    }
}
