package com.gausslab.weatherapi.util

import com.gausslab.weatherapi.core.util.LatLngValidator
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.Assertions.assertEquals

class LatLngValidatorTest {

    private val latLngValidator = LatLngValidator

    @Test
    fun `latitude가 float값이 들어오면 true`() {
        val latitude = 37.7749f

        val result = latLngValidator.isLatitudeValid(latitude)
        assertEquals(true, result)
    }

    @Test
    fun `longitude가 float값이 들어오면 true`() {
        val longitude = -122.4194f

        val result = latLngValidator.isLongitudeValid(longitude)
        assertEquals(true, result)
    }

    @Test
    fun `좌표가 모두 범위안에 있는 float값이 들어오면 true`() {
        val latitude = 37.7729f
        val longitude = -122.4194f

        val result = latLngValidator.isCoordinatesValid(latitude, longitude)
        assertEquals(true, result)
    }

    @Test
    fun `latitude가 NaN일 때 IllegalArgumentException`() {
        val latitude = Float.NaN

        assertThrows<IllegalArgumentException> {
            latLngValidator.isLatitudeValid(latitude)
        }
    }

    @Test
    fun `longitude가 NaN일 때 IllegalArgumentException`() {
        val longitude = Float.NaN

        assertThrows<IllegalArgumentException> {
            latLngValidator.isLongitudeValid(longitude)
        }
    }

    @Test
    fun `잘못된 latitude를 넣으면 IllegalArgumentException`() {
        val latitude = 100.0f

        assertThrows<IllegalArgumentException> {
            latLngValidator.isLatitudeValid(latitude)
        }
    }

    @Test
    fun `잘못된 longitude를 넣으면 IllegalArgumentException`() {
        val longitude = 356.0f

        assertThrows<IllegalArgumentException> {
            latLngValidator.isLongitudeValid(longitude)
        }
    }
}