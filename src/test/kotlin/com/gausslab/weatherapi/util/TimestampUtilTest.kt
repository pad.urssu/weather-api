package com.gausslab.weatherapi.util

import com.gausslab.weatherapi.core.util.TimestampUtil
import com.gausslab.weatherapi.core.util.TimestampUtil.toStartOfDayEpochSeconds
import com.gausslab.weatherapi.core.util.TimestampUtil.parseDateToEpochSeconds
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.ZoneOffset

class TimestampUtilTest {

    private val timestampUtil = TimestampUtil

    @Test
    fun `timestamp format이 정수로 제대로 넣으면 true`() {
        val timestamp = "1674288000"
        val result = timestampUtil.isTimestampValid(timestamp)
        assertEquals(true, result)
    }

    @Test
    fun `정수로 된 string 값이 들어오면 timestamp로 변환`() {
        val timestamp = "1674288000"
        val result = timestamp.toStartOfDayEpochSeconds()
        assertEquals(1674288000L, result)
    }

    @Test
    fun `timestamp format이 date로 제대로 넣으면 true`() {
        val timestamp = "2023-07-12"
        val result = timestampUtil.isTimestampValid(timestamp)
        assertEquals(true, result)
    }

    @Test
    fun `String을 Unix Epoch Time으로 변환 (기본 타임존)`() {
        val timestamp = "2023-07-12"
        val expectedTimestamp = 1689120000L // 해당 날짜의 Unix Epoch Time (UTC)
        val result = timestamp.parseDateToEpochSeconds()
        assertEquals(expectedTimestamp, result)
    }

    @Test
    fun `String을 Unix Epoch Time으로 변환 (다른 타임존)`() {
        val timestamp = "2023-07-12"
        val expectedTimestamp = 1689087600L // 해당 날짜의 Unix Epoch Time (Asia/Seoul 타임존)
        val result = timestamp.parseDateToEpochSeconds(ZoneOffset.ofHours(9))
        assertEquals(expectedTimestamp, result)
    }

    @Test
    fun `timestamp format이 올바르지 않은 경우 IllegalArgumentException`() {
        val timestamp = "2023/07/12"
        assertThrows<IllegalArgumentException> {
            timestampUtil.isTimestampValid(timestamp)
        }
    }

    @Test
    fun `timestamp변환시 format이 올바르지 않은 경우 IllegalArgumentException`() {
        val timestamp = "2023/07/12"
        assertThrows<IllegalArgumentException> {
            timestamp.parseDateToEpochSeconds()
        }
    }

    @Test
    fun `입력값 epoch 시작범위 이전이면 IllegalArgumentException`() {
        val timestamp = "-1"
        assertThrows<IllegalArgumentException> {
            timestampUtil.isTimestampValid(timestamp)
        }
    }

    @Test
    fun `입력값은 32bit Long을 넘기면 IllegalArgumentException`() {
        val timestamp = "4294967296" // 2^32
        assertThrows<IllegalArgumentException> {
            timestamp.toStartOfDayEpochSeconds()
        }
    }

    @Test
    fun `입력값이 date 범위에서 벗어나면 IllegalArgumentException`(){
        val timestamp = "1093-01-01"
        val futureTimestamp = "2038-01-20"
        assertThrows<IllegalArgumentException> {
            timestampUtil.isTimestampValid(timestamp)
            timestampUtil.isTimestampValid(futureTimestamp)
        }
    }
}