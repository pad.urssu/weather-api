package com.gausslab.weatherapi.util

import com.gausslab.weatherapi.*
import com.gausslab.weatherapi.core.util.WeatherInfoValidator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate
import java.time.ZoneOffset

class WeatherInfoValidatorTest {
    private val weatherInfoValidator = WeatherInfoValidator

    @Test
    fun `latitude값이 범위를 float로 제대로 들어가면 true`() {
        val latitude = MIN_LATITUDE + 0.5f
        val result = weatherInfoValidator.isLatitudeValid(latitude)
        assertEquals(true, result)
    }

    @Test
    fun `longiitude값이 범위를 float로 제대로 들어가면 true`() {
        val longitude = MIN_LONGITUDE + 0.5f
        val result = weatherInfoValidator.isLongitudeValid(longitude)
        assertEquals(true, result)
    }

    @Test
    fun `좌표가 모두 범위안에 있는 float값이 들어오면 true`() {
        val latitude = MIN_LATITUDE + 0.5f
        val longitude = MIN_LONGITUDE + 0.5f
        val result = weatherInfoValidator.isCoordinatesValid(latitude, longitude)
        assertEquals(true, result)
    }

    @Test
    fun `latitude값이 지정 범위를 벗어나면 IllegalArgumentException`() {
        val underLatitude = MIN_LONGITUDE - 0.5f
        val overLatitude = MAX_LATITUDE + 0.5f
        assertThrows<IllegalArgumentException> {
            weatherInfoValidator.isLatitudeValid(underLatitude)
            weatherInfoValidator.isLatitudeValid(overLatitude)
        }
    }

    @Test
    fun `longiitude값이 지정 범위를 벗어나면 IllegalArgumentException`() {
        val underLongitude = MIN_LONGITUDE - 0.5f
        val overLongitude = MAX_LONGITUDE + 0.5f
        assertThrows<IllegalArgumentException> {
            weatherInfoValidator.isLatitudeValid(underLongitude)
            weatherInfoValidator.isLatitudeValid(overLongitude)
        }
    }

    @Test
    fun `timestamp값이 범위안에 있는 값이 들어오면 true`() {
        val timestamp = LocalDate.now().toString()
        val epochTimestamp = LocalDate.now().atStartOfDay(ZoneOffset.UTC).toEpochSecond().toString()
        val result = weatherInfoValidator.isTimestampValid(timestamp)
        val epochResult = weatherInfoValidator.isTimestampValid(epochTimestamp)

        assertEquals(true, result)
        assertEquals(true, epochResult)
    }

    @Test
    fun `timestamp값이 범위를 벗어나면 IllegalArgumentException`() {
        val underTimestamp = LocalDate.parse(HISTORICAL_MIN_DATE).minusDays(1).toString()
        val underEpochTimestamp = LocalDate.parse(HISTORICAL_MIN_DATE).minusDays(1).atStartOfDay(ZoneOffset.UTC).toEpochSecond().toString()
        val overTimestamp = WeatherInfoValidator.dateStartingPointLimit.plusDays(5).toString()
        val overEpochTimestamp = WeatherInfoValidator.dateStartingPointLimit.plusDays(5).atStartOfDay(ZoneOffset.UTC).toEpochSecond().toString()

        assertThrows<IllegalArgumentException> {
            weatherInfoValidator.isTimestampValid(underTimestamp)
            weatherInfoValidator.isTimestampValid(underEpochTimestamp)
            weatherInfoValidator.isTimestampValid(overTimestamp)
            weatherInfoValidator.isTimestampValid(overEpochTimestamp)
        }
    }

    @Test
    fun `forecastedTime 값이 형식에 맞으면 true`() {
        val validForecastedTime = "14:30"
        val result = weatherInfoValidator.isForecastedTimeValid(validForecastedTime)
        assertEquals(true, result)
    }

    @Test
    fun `forecastedTime 값이 형식에 맞지 않으면 false`() {
        val invalidForecastedTime = "25:00"
        val result = weatherInfoValidator.isForecastedTimeValid(invalidForecastedTime)
        assertEquals(false, result)
    }

    @Test
    fun `forecastedTime 값이 비어 있으면 false`() {
        val emptyForecastedTime = ""
        val result = weatherInfoValidator.isForecastedTimeValid(emptyForecastedTime)
        assertEquals(false, result)
    }

    @Test
    fun `startDate값이 endDate보다 이전이면 true`(){
        val startDate = 1688256000L
        val endDate = 1688259600L
        val result = weatherInfoValidator.isDateValid(startDate, endDate)
        assertEquals(true, result)
    }
}