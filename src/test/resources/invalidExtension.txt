@RestController
class CsvReaderController(
        private val csvReaderService: CsvReaderService
) {
    @PostMapping("/api/weather/csv")
    suspend fun saveCsvFile(
            @RequestPart(required = true) csvFile: MultipartFile
    ): ResponseEntity<String> {
        val result = csvReaderService.saveCsvFile(csvFile)
        return if( result == "CSV file saved successfully.")
            ResponseEntity.ok(result)
        else
            ResponseEntity.badRequest().body(result)
    }
}

지금은 result 값이  result 값이 만약